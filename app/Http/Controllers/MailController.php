<?php

namespace App\Http\Controllers;
use App\Mail\SendEmail;
use App\Jobs\SendEmailJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail()
     { dispatch(new SendEmailJob);
   
  }

}
